﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System;
using Microsoft.AspNetCore.Diagnostics;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices
{
    public class CustomerServiceGrpc : Customer.CustomerBase
    {
        private readonly ILogger<CustomerServiceGrpc> _logger;
        private readonly IRepository<Core.Domain.PromoCodeManagement.Customer> _customerRep;
        private readonly IRepository<Preference> _preferenceRep;

        public CustomerServiceGrpc(ILogger<CustomerServiceGrpc> log
            , IRepository<Core.Domain.PromoCodeManagement.Customer> customerRep
            , IRepository<Preference> preferenceRep)
        {
            _logger = log;
            _customerRep = customerRep;
            _preferenceRep = preferenceRep;
        }

        public override async Task<CustomerShortResponseListGrpc> GetCustomers(NoParameter request, ServerCallContext context)
        {
            _logger.LogInformation("[gRPC] invoke function GetCustomers");

            var customerShortResponseListGrpc = new CustomerShortResponseListGrpc();
            var customers = await _customerRep.GetAllAsync();
            foreach (Core.Domain.PromoCodeManagement.Customer customer in customers)
            {
                customerShortResponseListGrpc.CustomersShortResponse.Add(new CustomerShortResponseGrpc()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                });
            }

            return customerShortResponseListGrpc;
        }

        public override async Task<CustomerResponseGrpc> GetCustomer(CustomerId request, ServerCallContext context)
        {
            _logger.LogInformation("[gRPC] invoke function GetCustomer");

            CustomerResponseGrpc CustomerResponseGrpc = new CustomerResponseGrpc();
            var customer = await _customerRep.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");
            else
            {
                var customerGrpc = new CustomerResponseGrpc()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                };

                if (customer.Preferences.Any())
                {
                    customerGrpc.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponseGrpc()
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name
                    }));
                }

                return customerGrpc;
            }
        }

        public override async Task<CustomerResponseGrpc> CreateCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            _logger.LogInformation("[gRPC] invoke function CreateCustomer");

            var preferences = await _preferenceRep.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            Core.Domain.PromoCodeManagement.Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRep.AddAsync(customer);

            var res = new CustomerResponseGrpc()
            {
                Id = customer.Id.ToString()
            };

            return res;

        }

        public override async Task<CustomerResponseGrpc> EditCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            _logger.LogInformation("[gRPC] invoke function EditCustomer");

            var customer = await _customerRep.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");

            var preferences = await _preferenceRep.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            customer = CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRep.UpdateAsync(customer);

            var customerGrpc = await GetCustomer(new CustomerId() { Id = request.Id }, context);

            return customerGrpc;
        }

        public override async Task<NoParameter> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
            _logger.LogInformation("[gRPC] invoke function DeleteCustomer");

            var customer = await _customerRep.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");

            await _customerRep.DeleteAsync(customer);

            return new NoParameter();
        }
    }
}
